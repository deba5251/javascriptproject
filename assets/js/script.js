const form = document.querySelector('form');

function handleSubmit(e) {
    e.preventDefault();
    const user = {
        firstName: form.querySelector('#fName').value,
        lastName: form.querySelector('#lName').value,
        email: form.querySelector('#emailId').value,
        password: form.querySelector('#pass').value,
    }

    if (user.firstName === '') {
        addErrorTo('fName');
    } else {
        removeErrorFrom('fName');
    }

    if (user.lastName === '') {
        addErrorTo('lName');
    } else {
        removeErrorFrom('lName');
    }

    if (user.email === '') {
        addErrorTo('emailId');
    } else if (!isValid(user.email)) {
        addErrorTo('emailId');
    } else {
        removeErrorFrom('emailId');
    }

    if (user.password === '') {
        addErrorTo('pass');
    } else {
        removeErrorFrom('pass');
    }
    if (user.firstName !== '' && user.lastName !== '' && user.email !== '' && user.password !== '') {
        localStorage.setItem('form_value', JSON.stringify(user))
        alert(`please check your value in local storage!`)
    }
}

function addErrorTo(field) {
    const formControl = form[field].parentNode;
    formControl.classList.add('error');
}

function removeErrorFrom(field) {
    const formControl = form[field].parentNode;
    formControl.classList.remove('error');
}

function isValid(email) {
    const re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return re.test(String(email).toLowerCase());
}

form.addEventListener('submit', handleSubmit);